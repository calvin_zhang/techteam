# README #

Welcome to the TechTeam repo.  This is our initial stab at capturing configuration and etc. for future automation.

### What is this repository for? ###

* This repository in it's current form as of 03/09/2016 is meant to be a bridge from our tribal knowledge to our future automation.
* This doesn't have a version number at this time.

### How do I get set up? ###

* There is no configuration requirements, as this manages other files configuration.  Salt is involved on this host.
* Dependencies include git (BitBucket) and SSH.
* As of 03/09/2016 there is no database involved.
* As of 03/09/2016 the testing process is a manual check that the files in question were deloyed as we expected.
* As of 03/09/2016 the deployment process is all by hand with an eye toward automation in the very near future.

### Contribution guidelines ###

Please reach out to **Berry Sizemore** to discuss your contribution and where it fits in.

### Who do I talk to? ###

Please reach out to **Berry Sizemore** or **Scott Thornson** for information about this repository.  Doug and Cody could be contacted as a last resort.

##TO DO##

* Create Monitors
* Instantiate Log Rotation
* Update Documentation
* Add flowchart images

JIRA:  x
DevOps:  x
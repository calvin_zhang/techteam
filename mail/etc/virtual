# 
# VIRTUAL(5)                                             VIRTUAL(5)
# 
# NAME
#        virtual - format of Postfix virtual table
# 
# SYNOPSIS
#        postmap /etc/postfix/virtual
# 
# DESCRIPTION
#        The  optional virtual table specifies address redirections
#        for local and non-local recipients or domains.  The  redi-
#        rections  are  used by the cleanup(8) daemon. The redirec-
#        tions are recursive.
# 
#        The virtual redirection is applied only to recipient enve-
#        lope  addresses,  and  does  not  affect  message headers.
#        Think Sendmail rule set S0, if you like. Use  canonical(5)
#        mapping  to  rewrite header and envelope addresses in gen-
#        eral.
# 
#        Normally, the virtual table is specified as  a  text  file
#        that  serves  as  input  to  the  postmap(1) command.  The
#        result, an indexed file in dbm or db format, is  used  for
#        fast  searching  by  the  mail system. Execute the command
#        postmap  /etc/postfix/virtual  in  order  to  rebuild  the
#        indexed file after changing the text file.
# 
#        When  the  table  is provided via other means such as NIS,
#        LDAP or SQL, the same lookups are  done  as  for  ordinary
#        indexed files.
# 
#        Alternatively,  the  table  can  be provided as a regular-
#        expression map where patterns are given as regular expres-
#        sions.  In  that  case, the lookups are done in a slightly
#        different way as described below.
# 
# POSTFIX-STYLE VIRTUAL DOMAINS
#        With a Postfix-style virtual domain,  the  virtual  domain
#        has  its  own  user  name  space. Local (i.e. non-virtual)
#        usernames are  not  visible  in  a  Postfix-style  virtual
#        domain.  In particular, local aliases(5) and mailing lists
#        are not visible as localname@virtual.domain.
# 
#        Use a Sendmail-style virtual domain (see below)  if  local
#        usernames,  aliases(5)  or mailing lists should be visible
#        as localname@virtual.domain.
# 
#        Support for a Postfix-style virtual domain looks like:
# 
#        /etc/postfix/virtual:
#            virtual.domain       anything (right-hand content does not matter)
#            postmaster@virtual.domain    postmaster
#            user1@virtual.domain address1
#            user2@virtual.domain address2, address3
# 
#        The virtual.domain anything entry is required for a  Post-
#        fix-style virtual domain.
# 
#        Do  not list a Postfix-style virtual domain in the main.cf
#        mydestination configuration parameter.  Such an  entry  is
#        required only for a Sendmail-style virtual domain.
# 
#        With  a  Postfix-style  virtual  domain,  the Postfix SMTP
#        server  accepts  mail  for  known-user@virtual.domain  and
#        rejects mail for unknown-user@virtual.domain as undeliver-
#        able.
# 
# SENDMAIL-STYLE VIRTUAL DOMAINS
#        With a Sendmail-style virtual domain,  every  local  (i.e.
#        non-virtual) username is visible in the virtual domain. In
#        particular, every local alias and mailing list is  visible
#        as localname@virtual.domain.
# 
#        Use  a  Postfix-style  virtual domain (see above) if local
#        usernames, aliases(5) or mailing lists should not be visi-
#        ble as localname@virtual.domain.
# 
#        Support for a Sendmail-style virtual domain looks like:
# 
#        /etc/postfix/main.cf:
#            mydestination = $myhostname localhost.$mydomain $mydomain
#                virtual.domain
# 
#        /etc/postfix/virtual:
#            user1@virtual.domain address1
#            user2@virtual.domain address2, address3
# 
#        The  main.cf  mydestination  entry is required for a Send-
#        mail-style virtual domain.
# 
#        Do not specify a virtual.domain anything virtual map entry
#        for  a  Sendmail-style  virtual  domain.  Such an entry is
#        required only with a Postfix-style virtual domain.
# 
#        With a Sendmail-style virtual domain,  the  Postfix  local
#        delivery  agent  delivers  mail  for  an unknown user@vir-
#        tual.domain to a local (i.e.  non-virtual) user  that  has
#        the  same  name;  if no such recipient exists, the Postfix
#        local delivery agent bounces the mail to the sender.
# 
# TABLE FORMAT
#        The format of the virtual table is  as  follows,  mappings
#        being tried in the order as listed in this manual page:
# 
#        pattern result
#               When  pattern matches a mail address, replace it by
#               the corresponding result.
# 
#        blank lines and comments
#               Empty lines and whitespace-only lines are  ignored,
#               as  are  lines whose first non-whitespace character
#               is a `#'.
# 
#        multi-line text
#               A logical line starts with non-whitespace  text.  A
#               line  that starts with whitespace continues a logi-
#               cal line.
# 
#        With lookups from indexed files such as DB or DBM, or from
#        networked  tables  such  as NIS, LDAP or SQL, patterns are
#        tried in the order as listed below:
# 
#        user@domain address, address, ...
#               Mail for  user@domain  is  redirected  to  address.
#               This form has the highest precedence.
# 
#        user address, address, ...
#               Mail  for  user@site  is redirected to address when
#               site is equal to $myorigin, when site is listed  in
#               $mydestination,   or   when   it   is   listed   in
#               $inet_interfaces.
# 
#               This functionality overlaps with  functionality  of
#               the local alias(5) database. The difference is that
#               virtual  mapping  can  be  applied   to   non-local
#               addresses.
# 
#        @domain address, address, ...
#               Mail  for  any  user  in  domain  is  redirected to
#               address.  This form has the lowest precedence.
# 
#        In all the above forms, when address has the form  @other-
#        domain,  the result is the same user in otherdomain.  This
#        works for the first address in the expansion only.
# 
# ADDRESS EXTENSION
#        When a mail address localpart contains the optional recip-
#        ient  delimiter  (e.g., user+foo@domain), the lookup order
#        becomes: user+foo@domain, user@domain, user+foo, user, and
#        @domain.   An unmatched address extension (+foo) is propa-
#        gated to the result of table lookup.
# 
# REGULAR EXPRESSION TABLES
#        This section describes how the table lookups  change  when
#        the table is given in the form of regular expressions. For
#        a description of regular expression lookup  table  syntax,
#        see regexp_table(5) or pcre_table(5).
# 
#        Each  pattern  is  a regular expression that is applied to
#        the entire address being looked up. Thus, user@domain mail
#        addresses  are  not  broken up into their user and @domain
#        constituent parts, nor is user+foo broken up into user and
#        foo.
# 
#        Patterns  are  applied  in  the  order as specified in the
#        table, until a pattern is found that  matches  the  search
#        string.
# 
#        Results  are  the  same as with indexed file lookups, with
#        the additional feature that parenthesized substrings  from
#        the pattern can be interpolated as $1, $2 and so on.
# 
# BUGS
#        The  table format does not understand quoting conventions.
# 
# CONFIGURATION PARAMETERS
#        The following main.cf parameters are  especially  relevant
#        to  this  topic.  See  the Postfix main.cf file for syntax
#        details and for default values.  Use  the  postfix  reload
#        command after a configuration change.
# 
#        virtual_maps
#               List of virtual mapping tables.
# 
#        Other parameters of interest:
# 
#        inet_interfaces
#               The  network  interface  addresses that this system
#               receives mail on.
# 
#        mydestination
#               List of domains that  this  mail  system  considers
#               local.
# 
#        myorigin
#               The domain that is appended to locally-posted mail.
# 
#        owner_request_special
#               Give special treatment to owner-xxx and xxx-request
#               addresses.
# 
# SEE ALSO
#        cleanup(8) canonicalize and enqueue mail
#        postmap(1) create mapping table
#        pcre_table(5) format of PCRE tables
#        regexp_table(5) format of POSIX regular expression tables
# 
# LICENSE
#        The  Secure  Mailer  license must be distributed with this
#        software.
# 
# AUTHOR(S)
#        Wietse Venema
#        IBM T.J. Watson Research
#        P.O. Box 704
#        Yorktown Heights, NY 10598, USA
# 
#                                                                 1
# 
#privacy@yourschoolshop.com	yss_privacy
#privacy@alumniplus.com		ap_privacy
#privacy@collegiateplus.com	csr
#ceo@collegiateplus.com		cp_ceo
#help@collegiateplus.com		customerservice
#info@collegiateplus.com		customerservice
suggestions@collegiateplus.com	customerservice
ppc@collegiateplus.com		ppc
ppc@alumniplus.com		ppc

# Greatergood.com
emailers@greatergood.com	emailers
security@greatergood.com	karen
#unsubscribestc@greatergood.com	stc
jobs@greatergood.com		jobs
employment@greatergood.com	jobs
eric@greatergood.com		eric
epierce@greatergood.com		epierce
lili@greatergood.com		lili
dogtoys@greatergood.com		ggdogt
800com@greatergood.com		gg800com
#harvest@greatergood.com		spam
bednbath@greatergood.com	bednbath
partnerinfo@greatergood.com	ggpartnerinfo
#ceo@greatergood.com		gg_ceo
news@greatergood.com		newsggc
newsites@greatergood.com	gg_newsites
partnerrelations@greatergood.com	ggpartnerrelations
spydermaster@greatergood.com	spydermaster
partnerinquiry@greatergood.com	partnerinquiry
remindme@greatergood.com		remind_gg
#privacy@greatergood.com			gg_privacy
#unsubscribe@greatergood.com 		csr
sponsor@greatergood.com			hs_sponsor
corporategifts@greatergood.com		sheila
customerservice@greatergood.com		csr
noreplies@greatergood.com		jeremy@jasmere.com
goodness@greatergood.com	mark

#info@alumniplus.com	apinfo
#help@alumniplus.com	aphelp
suggestions@alumniplus.com	apsuggestions
customerservice@alumniplus.com	apcustomerservice
dogtoys@yourschoolshop.com	yssdogt
dogtoys@alumniplus.com	apdogt
800com@yourschoolshop.com	yss800
800com@alumniplus.com	ap800
ap_areports@alumniplus.com	apamazon
ap_ereports@alumniplus.com	apetoy
bednbath@yourschoolshop.com	yssbandd
bednbath@alumniplus.com	apbedb
superb@yourschoolshop.com	ysssb
superb@alumniplus.com	apsb
ssreports@yourschoolshop.com	yssss
ssreports@alumniplus.com	apss
omreports@yourschoolshop.com	yssomax
omreports@alumniplus.com	apomax
link@yourschoolshop.com	ysslinks
link@alumniplus.com	laplus
gfreports@yourschoolshop.com	yssgfood
gfreports@alumniplus.com	apfood
dchef@yourschoolshop.com	yssdchef
dchef@alumniplus.com	apdchef
orvis@yourschoolshop.com	yssorvis
orvis@alumniplus.com	aporvis
#info@alumnigiving.com	tara@alumnigiving.com

bugs@alumniplus.com	apbug
bug@alumniplus.com	apbug
bugs@yss.com	yssbug
bugs@yourschoolshop.com	yssbug
bug@yss.com	yssbug
bug@yourschoolshop.com	yssbug
bug@collegiateplus.com	cpbug
bugs@collegiateplus.com	cpbug
bug@thehungersite.com	hs_bug
bugs@thehungersite.com	hs_bug

yss_suggestion@yourschoolshop.com	yssug
merchant_suggestion@yourschoolshop.com	ymerch
#info@yourschoolshop.com	yinfo
find@yourschoolshop.com	yfind
#general@yourschoolshop.com	ygeneral
#help@yourschoolshop.com	yhelp
new-products@yourschoolshop.com	ynew
#orders@yourschoolshop.com	yorders
#pricing@yourschoolshop.com	ypricing
suggestions@yourschoolshop.com	ysuggest
customerservice@yourschoolshop.com	ycust
yss_suggestion@yss4.com	yssug
merchant_suggestion@yss4.com	ymerch
#info@yss4.com	yinfo
find@yss4.com	yfind
#general@yss4.com	ygeneral
#help@yss4.com	yhelp
new-products@yss4.com	ynew
#orders@yss4.com	yorders
#pricing@yss4.com	ypricing
suggestions@yss4.com	ysuggest
customerservice@yss4.com	ycust
yss_suggestion@yss3.com	yssug
merchant_suggestion@yss3.com	ymerch
#info@yss3.com	yinfo
find@yss3.com	yfind
#general@yss3.com	ygeneral
#help@yss3.com	yhelp
new-products@yss3.com	ynew
#orders@yss3.com	yorders
#pricing@yss3.com	ypricing
suggestions@yss3.com	ysuggest
customerservice@yss3.com	ycust
yss_suggestion@yss2.com	yssug
merchant_suggestion@yss2.com	ymerch
#info@yss2.com	yinfo
find@yss2.com	yfind
#general@yss2.com	ygeneral
#help@yss2.com	yhelp
new-products@yss2.com	ynew
#orders@yss2.com	yorders
#pricing@yss2.com	ypricing
suggestions@yss2.com	ysuggest
customerservice@yss2.com	ycust
yss_suggestion@yss.com	yssug
merchant_suggestion@yss.com	ymerch
#info@yss.com	yinfo
find@yss.com	yfind
#general@yss.com	ygeneral
#help@yss.com	yhelp
new-products@yss.com	ynew
#orders@yss.com	yorders
#pricing@yss.com	ypricing
suggestions@yss.com	ysuggest
customerservice@yss.com	ycust
partnerinfo@alumniplus.com	appartnerinfo
#ceo@alumniplus.com	ap_ceo
#ceo@yourschoolshop.com	yss_ceo
#ceo@gearthatgives.com	gtg_ceo
customerservice@gearthatgives.com	csr
news@gearthatgives.com	newsgtg
newsites@alumniplus.com	ap_newsites
#receipts@yss.com	yreceipt
#receipts@yss2.com	yreceipt
#receipts@yss3.com	yreceipt
#receipts@yss4.com	yreceipt
#receipts@yourschoolshop.com	yreceipt
#receipts@alumniplus.com	apreceipts
#receipts@storesthatgive.com	stgreceipts
customerservice@storesthatgive.com	stgcustomerservice
#
#info@thehungersite.com	ths
#site@thehungersite.com	ths
sponsor@thehungersite.com	hs_sponsor
#mail@thehungersite.com	ths
#memberfeedback@thehungersite.com	ths_memberfeedback
reminder@thehungersite.com	reminderths
accountconcern@thehungersite.com	ths@chimay.yss4.com
accountconcerns@thehungersite.com	ths@chimay.yss4.com
#
#info@thehungersite.org	ths
#site@thehungersite.org	ths
sponsor@thehungersite.org	hs_sponsor
#mail@thehungersite.org	ths
#privacy@thehungersite.org	ths
#
#info@thehungersite.net	ths
#site@thehungersite.net	ths
sponsor@thehungersite.net	hs_sponsor
#mail@thehungersite.net	ths
#
#site@hungersite.net	ths
#info@hungersite.net	ths
sponsor@hungersite.net	hs_sponsor
#mail@hungersite.net	ths
news@hungersite.com	newsths
#
#info@hungersite.org	ths
#site@hungersite.org	ths
sponsor@hungersite.org	hs_sponsor
#mail@hungersite.org	ths
news@hungersite.org	newsths
#
#info@hungersite.com	ths
#site@hungersite.com	ths
sponsor@hungersite.com	hs_sponsor
#mail@hungersite.com	ths
#
#atempest@thehungersite.com	atempesths@chimay.yss4.com
#privacy@thehungersite.com	ths
#privacy@hungersite.com		ths
news@thehungersite.com	newsths
#new@thehungersite.com	newsths
lsaffiliate@gourmetgiving.com	lsaffiliate1
#
#privacy@therainforestsite.com	csr
#info@therainforestsite.com	trs
#site@therainforestsite.com	trs
sponsor@therainforestsite.com	trs_sponsor
news@therainforestsite.com	newstrs
survey@therainforestsite.com	kandrade
reminder@therainforestsite.com	remindertrs
accountconcerns@therainforestsite.com	trs@chimay.yss4.com
#
#privacy@therainforestsite.org	csr
#info@therainforestsite.org	trs
#site@therainforestsite.org	trs
sponsor@therainforestsite.org	trs_sponsor
news@therainforestsite.org	newstrs
#
#privacy@therainforestsite.net	csr
#info@therainforestsite.net	trs
#site@therainforestsite.net	trs
sponsor@therainforestsite.net	trs_sponsor
news@therainforestsite.net	newstrs
#
#privacy@rainforestsite.com	csr
#info@rainforestsite.com		trs
#site@rainforestsite.com		trs
sponsor@rainforestsite.com	trs_sponsor
news@rainforestsite.com	newstrs
#
#
#privacy@rainforestsite.net	csr
#info@rainforestsite.net		trs
#site@rainforestsite.net		trs
sponsor@rainforestsite.net	trs_sponsor
news@rainforestsite.net	newstrs
#
#privacy@rainforestsite.org	csr
#info@rainforestsite.org		trs
#site@rainforestsite.org		trs
sponsor@rainforestsite.org	trs_sponsor
news@rainforestsite.org		newstrs
#
#privacy@thekidsaidssite.com	csr
#info@thekidsaidssite.com	kas
#site@thekidsaidssite.com	thorson
sponsor@thekidsaidssite.com	ka_sponsor
news@thekidsaidssite.com	kas
#
#privacy@kidsaidssite.com	csr
info@kidsaidssite.com		kas
#site@kidsaidssite.com		thorson
sponsor@kidsaidssite.com	ka_sponsor
news@kidsaidssite.com		kas
#
#privacy@thepedaidssite.com	csr
#info@thepedaidssite.com		kas
#site@thepedaidssite.com		thorson
sponsor@thepedaidssite.com	ka_sponsor
news@thepedaidssite.com		kas
#
#privacy@thekidsaidsite.com	csr
#info@thekidsaidsite.com		kas
#site@thekidsaidsite.com		kas
sponsor@thekidsaidsite.com	ka_sponsor
news@thekidsaidsite.com		csr
#
#privacy@thekidshivsite.com	csr
#info@thekidshivsite.com		kas
#site@thekidshivsite.com		thorson
sponsor@thekidshivsite.com	ka_sponsor
news@thekidshivsite.com		csr
#
#privacy@kidsaidsite.com		csr
#info@kidsaidsite.com		kas
#site@kidsaidsite.com		thorson
sponsor@kidsaidsite.com		ka_sponsor
news@kidsaidsite.com		kas
# The Child Site
#privacy@thechildsurvivalsite.com	css
#info@thechildsurvivalsite.com		css
#site@thechildsurvivalsite.com		thorson
sponsor@thechildsurvivalsite.com	css
news@thechildsurvivalsite.com		newscss
accounts@thechildsurvivalsite.com	returned
accounts@childsurvivalsite.com	returned
# The BreastCancer Site
#privacy@thebreastcancersite.com		csr
#info@thebreastcancersite.com		bcs
#site@thebreastcancersite.com		thorson
sponsor@thebreastcancersite.com		bcs_sponsor
news@thebreastcancersite.com		bnews
#new@thebreastcancersite.com		bnews
#reminder@thebreastcancersite.com	bcs@chimay.yss4.com
reminder@thebreastcancersite.com	reminderbcs
accountconcerns@thebreastcancersite.com	bcs@chimay.yss4.com
accountconcern@thebreastcancersite.com	bcs@chimay.yss4.com
mystory@thebreastcancersite.com		jessicaf
customerservice@thebreastcancersite.com	csr

#privacy@thelandminesite.com		csr
#info@thelandminesite.com		tls
#site@thelandminesite.com		thorson
sponsor@thelandminesite.com		tls
news@thelandminesite.com		newslms
#reminder@thelandminesite.com		tls@chimay.yss4.com
accountconcerns@thelandminesite.com	tls@chimay.yss4.com

remindme@thehungersite.com		remind_ths
remindme@therainforestsite.com		remind_trs
remindme@thebreastcancersite.com	remind_bcs

customerservice@theanimalrescuesite.com	csr
communityadmin@theanimalrescuesite.com	jami
moderator@theanimalrescuesite.com	jami
#privacy@theanimalrescuesite.com		csr
#info@theanimalrescuesite.com		csr
news@theanimalrescuesite.com		csr
sponsor@theanimalrescuesite.com		arssponsor
reminder@theanimalrescuesite.com	reminderars
mystory@theanimalrescuesite.com		jessicaf
ecards@theanimalrescuesite.com		jessicaf
amazinganimals@theanimalrescuesite.com	jessicaf
love@theanimalrescuesite.com		editorial

accountconcern@thechildhealthsite.com	csr
accountconcerns@thechildhealthsite.com	csr
customerservice@thechildhealthsite.com	csr
#info@thechildhealthsite.com	csr
#memberfeedback@thechildhealthsite.com	csr
news@thechildhealthsite.com	newschs
#privacy@thechildhealthsite.com	csr
reminder@thechildhealthsite.com	csr
#site@thechildhealthsite.com	csr
sponsor@thechildhealthsite.com	chssponsor

#unsubscribe@thehungersite.com		csr
#unsubscribe@thebreastcancersite.com	csr
#unsubscribe@thechildhealthsite.com	csr
#unsubscribe@therainforestsite.com	csr
#unsubscribe@theanimalrescuesite.com	csr
#unsubscribe@gearthatgives.com		csr
#unsubscribe@charitylist.com 		csr
#unsubscribe@charitylist.org 		csr


accountconcern@charitylist.com		csr
accountconcerns@charitylist.com		csr
customerservice@charitylist.com		csr
#info@charitylist.com			csr
#memberfeedback@charitylist.com		csr
news@charitylist.com			newschl
#privacy@charitylist.com			csr
reminder@charitylist.com		csr
#site@charitylist.com			csr
support@charitylist.com			csr

accountconcern@charitylist.org		csr
accountconcerns@charitylist.org		csr
customerservice@charitylist.org		csr
#info@charitylist.org			csr
#memberfeedback@charitylist.org		csr
news@charitylist.org			newschl
#privacy@charitylist.org			csr
reminder@charitylist.org		csr
#site@charitylist.org			csr
support@charitylist.org			csr
HelpACause@CharityList.org 		csr
HelpACause@CharityList.com 		csr


# museumshop.com
#unsubscribe@museumshop.com		csr
returns@museumshop.com			csr
corporategifts@museumshop.com		csr
bulkpurchases@museumshop.com		csr
news@museumshop.com			newsmuseum
sales@museumshop.com			noaddress

# skitch.net fuckers
steven@skitch.net			thorson
inventory01@skitch.net			thorson
inventory@skitch.net			thorson
skitch@skitch.net			thorson
steve@skitch.net			thorson
john@skitch.net			thorson

# Literacysite
reminder@theliteracysite.com		reminderlit
sponsor@theliteracysite.com		litsponsor

# Greatergood Network
customerservice@greatergoodnetwork.com	csrggn

# New look account for brian
newlook@greatergood.com			newlookaccount
newlook@thehungersite.com		newlookaccount
newlook@theanimalerescuesite.com	newlookaccount
newlook@thebreastcancersite.com		newlookaccount
newlook@therainforestsite.com		newlookaccount
newlook@thechildhealthsite.com		newlookaccount
newlook@theliteracysite.com		newlookaccount

# geckotraders
info@geckotraders.com			wholesale
info@geckotraders.org			wholesale

# charityusa.com

CharityUSABuyers@charityusa.com		sarah,suzanne,gabi
customerservice@charityusa.com		returned

# greatergood.org

info@greatergood.org			allisonwexler,dafny
#info@greatergood.org			: /dev/null
staff@greatergood.org			lizbaker,sallyriggs,noahhorton,jimkober,susan.rosenberg,johnkane,elizabethasher,susanvilsack,katispillane,katherineroper,brynadonnelly,denisebash,seancherry,dafny,karla.ball,jwaterfallkanter,brittanyjohn,lisellecomstock,kelseycorley,kira,elenatownsend,sarahkroh

rescuerebuild@greatergood.org		noahhorton
updates@greatergood.org			hannah


# ecologyfund
info@ecologyfund.com			matt
webmaster@ecologyfund.com		matt
comments@ecologyfund.com		matt
feedback@ecologyfund.com		matt

# veteranssite

reminder@theveteranssite.com		remindervet
sponsor@theveteranssite.com		vetsponsor

# diabetessite
reminder@thediabetessite.com		reminderdbs

# globalfirlfriend

info@globalgirlfriend.com	infoggc
orders@globalgirlfriend.com	ggforders
dvo@globalgirlfriend.com	ggforders

# goodtube.org

kimberly@goodtube.org		kimberly.klintworth@gmail.com
lynn@goodtube.org		lynnpeterson@greatergood.org

# onepicturesaves.com
mail@onepicturesaves.com	chowells
info@onepicturesaves.com	opsinfo

# catsrcool.org
info@catsrcool.org		chowells

# Shelter Challenge
admin@shelterchallenge.com	rosemary,nikki
press@shelterchallenge.com	rosemary,nikki
unsubscribe@shelterchallenge.com	rosemary,nikki


# Greatergood Wholesale
orders@greatergoodwholesale.com	druce,jontell
info@greatergoodwholesale.com	brianne,druce,analamanna

# Faithub.net/org webrefinery.org
info@faithhub.org	mharrell
info@faithhub.net       mharrell
mail@faithhub.net       alec
info@webrefinery.org	alec

# thehappihouse.com

lynn@thehappihouse.com lynnpeterson
happi@thehappihouse.com lynnpeterson
becky@thehappihouse.com becky
peggy@thehappihouse.com peggy
kenneth@thehappihouse.com kenneth
kkearns@thehappihouse.com kkearns
jidelkope@thehappihouse.com jidelkope

# onemangotree.com

connor@onemangotree.com	sedgar
courtney@onemangotree.com	sedgar
halle@onemangotree.com	sedgar
info@onemangotree.com	sedgar
warehouse@onemangotree.com	sedgar

# Rescuebank
jk@rescuebank.org	johnkane
elizabeth@rescuebank.org	elizabethasher
sue@rescuebank.org	susanvilsack
info@rescuebank.org	susanvilsack
barbara@rescuebank.org	susanvilsack
craig@rescuebank.org	susanvilsack
inventory@rescuebank.org	susanvilsack
accountspayable@rescuebank.org	elizabethasher
recipientregistration@rescuebank.org	elizabethasher
leslie@rescuebank.org leslierandall
tara@rescuebank.org taratillmans
robin@rescuebank.org robingraham
sandy@rescuebank.org sandyschmidt
sheri@rescuebank.org sherisanders

# greatergoodness.com
info@greatergoodness.com  mark

# 12 tomatoes
chef@12tomatoes.com		sthomas@greatergood.com

# Free Kibble
info@freekibble.com		allison,kelly
happy@freekibble.com		allison
shoutout@freekibble.com		allison
mimi@freekibble.com		kelly
brooke@freekibble.com		kelly
